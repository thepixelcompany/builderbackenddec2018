const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({port: 4001});

const to = require('../util/to');
const NodeService = require('./../services/NodeService');
const {KPIService} = require('./../services/KPIService');
const NodeLogger = require('../logger/NodeLogger');


let idOfThisNode = '';

/**
 * Instantiates a new WebSocket object, that triggers different functions on opening and closing connections
 * or sending and receiving messages.
 *
 * @author Lukas
 */
wss.on('connection', async function (ws) {
    // let token = {token: ''}; // call by reference
    // onNewConnection(ws, token);
    // onReceiveMessage(ws, token);
    // onCloseConnection(ws, token);
    await onNewConnection(ws);
    await onReceiveMessage(ws);
    await onCloseConnection(ws);
});

/**
 * Gets triggered upon a new connection.
 *
 * @param {WebSocketServer-object} ws - WebSocket server object that gets activated upon a new connection
 * @author Lukas
 */
async function onNewConnection(ws) {
    sendMessageToClient(ws, 'connectionSuccess',null);
    await NodeLogger.writeInLogFile("WebSocket connection open at: " + new Date());
}

/**
 * Passes received messages on tho the messagehandler.
 *
 * @param {WebSocketServer-object} ws - WebSocket server object where messages can be received
 * @author Lukas
 */
function onReceiveMessage(ws) {
    ws.on('message', async function (message) {
        const messageJSON = safelyParseJSON(message);
        if (messageJSON !== '' && JSONcontainsAllRequiredAttributes(messageJSON) && isTimestampANumber(messageJSON)) {
            if (JSONcontainsNodeId(messageJSON)) {
                this.idOfThisNode = messageJSON['nodeID'];
                // console.log('The ID of this node is ' + this.idOfThisNode);
            }
            await messageHandler(ws, messageJSON);
        } else {
            await NodeLogger.writeErrorInLogFile('Message could not be parsed to JSON format ' +
                'or does not contain the required fields');
        }
    });
}

/**
 * Everything that gets fired when the connection gets closed.
 * currently empty
 *
 * @param {WebSocketServer-object} ws - WebSocket server object that gets triggered when closed
 * @author Lukas
 */
function onCloseConnection(ws) {
    ws.on("close", function () {
        // TODO delete the JWT
        // NodeService.turnNodeInactive(this.idOfThisNode);
        // console.log('Turn inactive: ' + this.idOfThisNode);
        NodeLogger.writeInLogFile('WebSocket connection closed at: ' + new Date());
    });
}

/**
 * Sends a message to the nodes.
 *
 * @param {WebSocketServer-object} ws - WebSocket server object where messages can be sent
 * @param {String} message - message that gets sent to the nodes
 * @param {String} infoText - info text that gets sent to the
 */
function sendMessageToClient(ws, messageType, messageContent) {
    // const timestamp = new Date().getTime();
    if (messageContent === null) {
        ws.send(JSON.stringify({
            messageType: messageType
        }));
    } else {
        ws.send(JSON.stringify({
            messageType: messageType,
            messageContent: messageContent
        }));
    }
}

/**
 * Handles every incoming message from the nodes and redirects it accordingly.
 *
 * @param {String} message - message from the nodes
 * @author Lukas
 */
async function messageHandler(ws, messageJSON) {
    switch (messageJSON.messageType) {
        case 'registration':
            let[errR, dataR] = await to(NodeService.registerNewNode(messageJSON));
            if (errR) {
                await NodeLogger.writeErrorInLogFile('Could not register a new node at: ' + new Date());
            } else {
                await NodeLogger.writeInLogFile(dataR.name + ' got successfully added to the DB with ID ' + dataR._id);
                sendMessageToClient(ws, 'registrationSuccess', dataR);
            }
            break;
        case 'openConnection':
            let[errOC, dataOC] = await to(NodeService.openConnection(messageJSON.nodeID));
            if (errOC) {
                await NodeLogger.writeErrorInLogFile('Could not open a new connection at: ' + new Date());
            } else {
                // await NodeService.openConnection(messageJSON.nodeID);
                sendMessageToClient(ws, 'connectionOpened', null);
            }
            break;
        case 'kpi':
            let [errK, dataK] = await to(KPIService.storeKPIfromNode(messageJSON));
            if (errK) {
                await NodeLogger.writeErrorInLogFile('Could not write new KPI message to DB at: ' + new Date());
            } else {
                // await KPIService.storeKPIfromNode(messageJSON);
                // fliegt später wieder raus, is grad nur zum testen
                sendMessageToClient(ws, 'kpiSuccess', null);
            }
            break;
        case 'configsChanged':
            let [errC, dataC] = await to(NodeService.configsChanged(messageJSON));
            if (errC) {
                await NodeLogger.writeErrorInLogFile('Could not process the change of configurations at: ' + new Date());
            } else {
                await NodeLogger.writeInLogFile('Node configurations got changed');
            }
            break;
        default:
            await NodeLogger.writeErrorInLogFile('Unexpected message type --> message was dropped at: ' + new Date());
    }
}

/**
 * Parses a String to a JSON.
 *
 * @param {String} json - string that is about to be parsed to JSON
 * @returns {string} - parsed JSON
 * @author Lukas
 */
function safelyParseJSON(json) {
    let parsed = '';
    try {
        parsed = JSON.parse(json);
        // console.log('Parsed Json: ' + JSON.stringify(parsed));
        // console.log('ID: ' + parsed['nodeID']);
    } catch (err) {
        NodeLogger.writeErrorInLogFile('JSON from message could not be parsed at: ' + new Date())
    }
    return parsed;
}

/**
 * Checks if the Json message contains al required attributes to pass it on.
 *
 * @param {JSON} json - json message that has to be checked
 * @returns {boolean} - whether the json message contains all required key value pairs
 * @author Lukas
 */
function JSONcontainsAllRequiredAttributes(json) {
    // console.log('Inside JSONcontainsAllRequiredAttributes & returns: ' + !(json.messageType === undefined || json.timestamp === undefined));
    return !(json.messageType === undefined || json.timestamp === undefined);
}

/**
 * Checks if the Json message contains a nodeID.
 *
 * @param {JSON} json - json message that has to be checked
 * @returns {boolean} - whether the json message contains a nodeID
 * @author Lukas
 */
function JSONcontainsNodeId(json) {
    // console.log('Inside JSONcontainsNodeId & returns: ' + (json['nodeID'] !== undefined));
    return (json['nodeID'] !== undefined);
}

/**
 * Checks if the timestamp of the Json message is a number.
 *
 * @param {JSON} json - json message that has to be checked
 * @returns {boolean} - whether the timestamp of the Json message is a number
 * @author Lukas
 */
function isTimestampANumber(json) {
    console.log('Inside isTimestampANumber & returns: ' + !isNaN(json['timestamp']));
    return !isNaN(json['timestamp']);
}



console.log('-> WebSocket is running on port 4001');
module.exports = {wss, sendMessageToClient};