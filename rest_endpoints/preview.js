const express = require('express');
const fs = require('fs');
const router = express.Router();
var path = require("path");
var Htmlbeautify = require('js-beautify').html
var Cssbeautify = require('js-beautify').css
var Jsbeautify = require('js-beautify').js
router.post('/', (req, res) => {
    let Source = __dirname + '/../upload/src/';
    let htmlCode = Htmlbeautify(req.body.html, {indent_size: 2, space_in_empty_paren: true});
    let cssCode = Cssbeautify(req.body.css, {indent_size: 2, space_in_empty_paren: true});    
    let jsCode = Jsbeautify(req.body.js, {indent_size: 2, space_in_empty_paren: true});
    fs.writeFile(Source + 'index.html', htmlCode, (err, html) => {
        fs.writeFile(Source + 'style.css', cssCode, (err, css) => {
            fs.writeFile(Source + 'main.js', jsCode, (err, js) => {
                let data = {html: html, css: css, js: js}
                res.send(data)
            })
        })
    })
    
})
router.post('/update', (req, res) => {
    let Source = __dirname + '/../upload/src/';
    if (req.body.type === 1) {
        Source += 'index.html'
    } else if (req.body.type === 2) {
        Source += 'style.css'
    } else {
        Source += 'main.js'
    }
    fs.writeFile(Source, req.body.code, (err, code) => {
        res.send(code)
    })
})
module.exports = router;