const express = require('express');
const router = express.Router();
const multer = require('multer')
var path = require("path");
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'upload/')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
const upload = multer({ storage: storage })
const mediaModel = require('../models/media.js');

router.post('/insert',  upload.single('mediaFile'), (req, res) => {
    let image_name = path.basename(req.file.path)
    let image_path = 'http://' + req.headers.host + '/' + image_name
    mediaModel.insert({image_name: image_name, image_path: image_path}).then(result => {
        mediaModel.last_row().then(field => {
            res.send(field)
        })
    }, err => {
        res.send(err)
    })
})
router.get('/get', (req, res) => {
    mediaModel.get().then(result => {
        res.send(result)
    }, err => {
        res.send(err)
    })
})
router.post('/delete', (req, res) => {
    mediaModel.delete(req.body.id).then( result => {
        res.send(result)
    }, err => {
        res.send(err)
    })
})
module.exports = router;
