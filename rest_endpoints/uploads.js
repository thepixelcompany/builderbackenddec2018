const express = require('express');
const router = express.Router();
const multer = require('multer')
var path = require("path");
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'upload/')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
const upload = multer({ storage: storage })
router.post('/', upload.single('myfile'), (req, res) => {
    console.log(req.headers.host)
    let filename = path.basename(req.file.path)
    console.log(req.file.path)
    let image_path = 'http://' + req.headers.host + '/' + filename
    console.log(image_path)
    res.send(image_path)
})
module.exports = router;