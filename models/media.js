var mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pixelcode'
})
const upload = {
    insert(data) {
        var sql = "INSERT INTO upload_tb (`image_name`, `image_path`) VALUES ('" + data.image_name + "', '" + data.image_path + "')"
        return new Promise((resolve, reject)=>{
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result)
                }
            }) 
        })
    },
    last_row() {
        var sql = "SELECT * FROM upload_tb ORDER BY id DESC LIMIT 1";
        return new Promise((resolve, reject) => {
            connection.query(sql, (err, field, row) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(field)
                }
            })
        })
    },
    get() {
        var sql = "SELECT * FROM upload_tb";
        return new Promise((resolve, reject) => {
            connection.query(sql, (err, field, row) => {
                if (err) {
                    reject (err)
                } else {
                    resolve(field)
                }
            })
        })
    },
    delete(id) {
        var sql = "DELETE FROM upload_tb WHERE id = " + id;
        return new Promise((resolve, reject) => {
            connection.query(sql, (err, result) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(result)
                }
            })
        })
    }
}

module.exports = upload