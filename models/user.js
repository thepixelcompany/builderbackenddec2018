const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;
const to = require('../util/to');
const bcrypt = require('bcryptjs');

const NotificationSchema = new Schema({
    alert: {
        type: ObjectId,
        required: true,
        ref: 'Alert'
    },
    node: {
        type: ObjectId,
        required: true,
        ref: 'Node'
    },
    message: {
        type: String,
        required: true
    }
});

const NodesLocalNameSchema = new Schema({
    node: {
        type: ObjectId,
        required: true,
        ref: 'Node'
    },
    name: {
        type: String,
        required: true
    }
});

const NodeGroupSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 40
    },
    nodes: [ObjectId]
});

const ConditionSchema = new Schema({
    type: {
        type: String,
        enum: ["goes above", "falls below", "stops sending"]
    },
    kpi_type: {
        type: String,
        enum: ["power usage", "CPU utilization", "RAM usage", "swap memory usage", "disk usage", "disk accesses", "disk throughput", "network throughput"]
    },
    data_function: {
        type: String,
        enum: ["derivative", "integral", "linear"]
    },
    threshold: {
        type: Number
    },
    time: {
        type: Number,
        required: true
    }
});


const AlertSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 1
    },
    nodes: [{
        type: ObjectId,
        ref: 'Node',
        required: true
    }],
    timeout: {
        type: Number,
        required: true,
        min: 0
    },
    email: {
        type: String,
        validate: {
            validator: validator.isEmail,
            message: 'Email is invalid {VALUE}'
        }
    },
    conditions: [ConditionSchema]
});


const UserSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: 'Entered an invalid email'
        }
    },
    password: {
        type: String,
        require: true,
        minlength: 5,
        select: false
    },
    role: {
        type: String,
        enum: ["basicUser", "extendedUser", "admin"]
    },
    dashb_upd_freq: {
        type: Number,
        require: true
    },
    notifications: [{
        type: NotificationSchema,
        default: undefined
    }],
    nodes_local_names: [{
        type: NodesLocalNameSchema,
        default: undefined
    }],
    alerts: [{
        type: AlertSchema,
        default: undefined
    }],
    nodes: [{
        type: ObjectId,
        ref: 'Node',
        default: undefined
    }],
    node_groups: {
        type: [NodeGroupSchema],
        default: undefined
    }
});

UserSchema.pre('findByIdAndUpdate', function (next) {
   this.options.runValidators = true;
   next();
});

UserSchema.pre('findOneAndUpdate', function (next) {
    this.options.runValidators = true;
    next();
});

UserSchema.methods.comparePassword = async function(password) {
    let user = this;

    let [err, isValid] = await to(bcrypt.compare(password, user.password));
    if (err) throw new Error(err);
    if (!isValid) throw new Error("Invalid password.");

    return user;
};

const User = mongoose.model("User", UserSchema);
const Alert = mongoose.model("Alert", AlertSchema);


module.exports = {User, Alert};