const _ = require('lodash');

let picker = (target, keys) => {
    if (_.isArray(target)) {
        target = target.map(e => _.pick(e, keys));
    } else if (_.isObject(target)) {
        target = _.pick(target, keys);
    }
    return target;
};

module.exports = {
    picker
};