const generator = require('generate-password');
const bcrypt = require('bcryptjs');
const to = require('../util/to');
const _ = require('lodash');

class Password {
    constructor(input) {
        if (_.isFinite(input) && input > 0) {
            this.password = this.generateRandomPassword(input);
        } else {
            this.password = input;
        }
    }

    generateRandomPassword(length) {
        let password = generator.generate({
            length: length,
            numbers: true,
            symbols: true
        });

        return password;
    }

    async hash() {
        let err, salt, hash;

        [err, salt] = await to(bcrypt.genSalt(10));
        if (err) throw new Error(err);

        [err, hash] = await to(bcrypt.hash(this.password, salt));
        if (err) throw new Error(err);

        return hash;
    }
}

module.exports = {Password};