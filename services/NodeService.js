'use strict';

const {Node} = require('../models/node');
const {Message} = require('./Message');
const to = require('../util/to');
const _ = require('lodash');
const NodeLogger = require('../logger/NodeLogger');
const {User} = require('../models/user');
const {picker} = require('../util/picker');

/**
 * This class includes the logic regarding nodes and retrieves appropriate nodes from the DB.
 */
class NodeService {

    /**
     * Retrieves all nodes of a specific user or all nodes in the DB at the specified page.
     *
     * @param {String} user - The user whose nodes should be retrieved. All nodes are returned if null.
     * @param {Number} page - The page number to look for.
     * @returns {Promise<{pages, data: *}>} - The nodes array if some exist, null otherwise.
     * @author Tim
     */
    static async getNodes(user, page) {
        page = _.toNumber(page);
        if (!(_.isFinite(page) && page > 0)) {
            page = 1;
        }

        let limit = 20;
        let skip = limit * page - limit;
        let err, nodeDocs, count;

        if (_.isString(user)) {
            [err, user] = await to(User.findById(user, {nodes: { $slice: [skip, limit] }, nodes_local_names: 1})
                .populate('nodes', '_id name active freq'));

            if (err) throw new Error(err);
            nodeDocs = user.nodes;
            count = nodeDocs.length;

            nodeDocs = nodeDocs.map(doc => {
                doc = doc.toObject();

                // TODO reduce array after every cycle
                const local_name = _.find(user.nodes_local_names, n => _.isEqual(n.node, doc._id));
                if (local_name) doc.local_name = local_name.name;

                return doc;
            });
        } else {
            [err, nodeDocs] = await to(Node.find({}, '_id name active freq', {skip, limit}));
            if (err) throw new Error(err);

            [err, count] = await to(Node.countDocuments());
        }

        let pagesCount = _.ceil(count / limit);

        if (err) {
            throw new Error(err);
        } else {
            return {data: nodeDocs, pages: pagesCount};
        }
    }

    /**
     * Retrieves one node with the specified id.
     *
     * @param {String} id - The id of the node to be retrieved.
     * @param {String} userId - The optional id of the user to lookup the name of the node which is defined by the user.
     * @returns {Promise<Object>} - The node object if exists, null otherwise.
     * @author Tim
     */
    static async getNode(id, userId) {
        let err, node, user;

        [err, node] = await to(Node.findById(id));
        if (err) {
            throw new Error(err.message);
        } else if (!node) {
            throw new Error(Message.NO_SUCH_NODE);
        } else {
            node = picker(node, ['_id', 'freq', 'name', 'active']);

            // Looks up the local name of a node which is defined by a user.
            if (_.isString(userId)) {
                [err, user] = await to(User.findById(userId, 'nodes_local_names'));
                if (err) throw new Error(err);

                const local_name = _.find(user.nodes_local_names, n => _.isEqual(n.node.toHexString(), id));
                if (local_name) node.local_name = local_name.name;
            }
            return {data: node};
        }
    }

    /**
     * Deletes the node with the specified id from the DB.
     *
     * @param {String} id - The id of the node to be deleted.
     * @returns {Promise<Object>} - The deleted node document
     * @author Tim
     */
    static async deleteNode(id) {
        let err, node;

        [err, node] = await to(Node.findByIdAndDelete(id));
        if (err) throw new Error(err);
        if (!node) throw new Error(Message.NO_SUCH_NODE);

        return {data: node};
    }

    /**
     * Updates the node with the specified id and sets the new data.
     *
     * @param {String} id - The id of the node to be updated.
     * @param {Object} data - The values of the node to be updated.
     * @returns {Promise<Object>} - The new node document if exists.
     * @author Tim
     */
    static async updateNode(id, data) {
        let err, node;
        data = _.pick(data, ['name', 'freq']);

        [err, node] = await to(Node.findOneAndUpdate({_id: id}, data, {new: true}));
        if (err) {
            throw new Error(err);
        } else if (!node) {
            throw new Error(Message.NO_SUCH_NODE);
        }

        return {data: node};
    }

    /**
     * Statically increases the number of nodes to have a consistent initial naming schema.
     *
     * @returns {number} - The node counter increased by one
     * @author Lukas
     */
    static nextNodeCount() {
        // Number.MAX_SAFE_INTEGER is 9007199254740991
        if (Number.MAX_SAFE_INTEGER === NodeService.staticNodeCounter) {
            NodeService.staticNodeCounter = 0;
            return ++NodeService.staticNodeCounter;
        } else {
            return ++NodeService.staticNodeCounter;
        }
    }

    /**
     * Returns the ip of a message.
     * currently hardcoded
     *
     * @returns {string} - ip address
     * @author Lukas
     */
    static getIpOfNode() {
        return "23.23.23.23";
    }

    /**
     * Returns the Port of a message
     * currently hardcoded
     *
     * @returns {number} - port number
     * @author Lukas
     */
    static getPortOfNode() {
        return 123;
    }

    /**
     * Returns the active state of a node.
     * currently hardcoded
     *
     * @returns {boolean} - active state
     * @author Lukas
     */
    static isThisNodeActive() {
        return true;
    }

    /**
     * Returns the kpi sending frequency of a node.
     * currently hardcoded
     *
     * @returns {number} - kpi sending frequency
     * @author Lukas
     */
    static getFrequencyOfNode() {
        return 12;
    }

    /**
     * Stores a new node in the database with the information from the message
     * and opens the connection.
     * currently hardcoded info
     *
     * @param {JSON} json - Json message from a node upon registration at the backend
     * @author Lukas
     */
    static async registerNewNode(json) {

        let newNode = new Node({
            _id: '5c04fc4b245fc703ecc66133', // ID ist hardcodiert, fliegt später wieder raus
            name: 'Node_' + NodeService.nextNodeCount().toString(),
            ip: NodeService.getIpOfNode(),
            port: NodeService.getPortOfNode(),
            active: NodeService.isThisNodeActive(),
            freq: this.getFrequencyOfNode()
        });

        let [err, data] = await to(newNode.save());
        if (err) throw new Error(err);
        return data;
    }

    /**
     * Handles the response from the nodes that send a changed configuration message.
     *
     * @param {JSON} json - Json message from a node upon registration at the backend
     * @author Lukas
     */
    static async configsChanged(json) {
        const nodeID = json.nodeID;
        console.log('Inside configsChanged - nodeID is ' + nodeID);

        // TODO handle the configsChanged response from the node

        /*
        if(await this.doesNodeExistWithId(nodeID)) {
            if (await this.isNodeActive(nodeID)) {
                // TODO change the configs
                // TODO pass info on to Frontend
            } else {
                // TODO dont change the configs, because the node is not active
                NodeLogger.writeInLogFile('Configs did not get changed, because node is not active')
            }
        } else {
            // TODO answer to Nodes, that node does not exist & has to register first
            NodeLogger.writeInLogFile('Node does not exist and has to get registered first to change the configs');
        }
        */

        console.log('This will be tackled in implementation B');
    }

    /**
     * Reopens the connection to a node that already exists in the database.
     *
     * @param {String} nodeID - Id of the node which connection gets reopened
     * @author Lukas
     */
    static async openConnection(nodeID) {
        if(await this.doesNodeExistWithId(nodeID)) {
            if (await this.isNodeActive(nodeID)) {
                await NodeLogger.writeInLogFile('The Node is still active');
            } else {
                let [err, data] = await to(this.turnNodeActive(nodeID));
                if (err) throw new Error(err);
                await NodeLogger.writeInLogFile('A new connection with the existing node got opened');
            }
        } else {
            // TODO answer to Nodes, that node does not exist & has to register first
            await NodeLogger.writeInLogFile('Node does not exist and has to get registered first to reopen the connection');
        }
    }

    /**
     * Checks is a node with a specific ID exists in the DB.
     *
     * @param nodeID
     * @returns {Promise<boolean>} - boolean whether node exists in DB or not
     */
    static async doesNodeExistWithId(nodeID) {
        return (await Node.findById(nodeID) !== null);
    }

    /**
     * Checks if the active state of a node with a specific Id is true or false.
     *
     * @param {String} nodeID - ID of a node
     * @returns {Promise<boolean>} - active state of the node from the DB
     */
    static async isNodeActive(nodeID) {
        return (await Node.findById(nodeID, 'active')).active;
    }

    /**
     * Turns the active state of a node to true in the DB.
     *
     * @param {String} nodeID - ID of a node
     * @author Lukas
     */
    static async turnNodeActive(nodeID) {
        let nodeDoc = await Node.findByIdAndUpdate(nodeID, {active: true}, {new: true});
        NodeLogger.writeInLogFile(nodeDoc.name + ' turned active to ' + nodeDoc.active);
    }

    /**
     * Turns the active state of a node to false in the DB.
     *
     * @param {String} nodeID - ID of a node
     * @author Lukas
     */
    static async turnNodeInactive(nodeID) {
        let nodeDoc = await Node.findByIdAndUpdate(nodeID, {active: false}, {new: true});
        NodeLogger.writeInLogFile(nodeDoc.name + ' turned active to ' + nodeDoc.active);
    }
}

/**
 * Static node counter for a consistent initial naming schema.
 *
 * @type {number} staticNodeCounter - Count of nodes written to DB
 */
// default counter for passing names to Nodes
NodeService.staticNodeCounter = 0;

module.exports = NodeService;