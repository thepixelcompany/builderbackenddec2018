const _ = require('lodash');
const {User, Alert} = require('../models/user');
const {Message} = require('./Message');
const to = require('../util/to');

class AlertService {
    static async createAlert(userId, data) {
        let err, alert, user;
        [err, user] = await to(User.findById(userId, 'alerts'));
        if (err) throw new Error(err);

        alert = new Alert(data);
        user.alerts.push(alert);

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: alert.toObject()};
    }

    static async getAlerts(userId, page) {
        const limit = 20;
        const skip = limit * page - limit;
        let err, user, count, pages;

        [err, user] = await to(User.findById(userId, 'alerts', {limit, skip}));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        count = user.alerts.length;
        pages = _.ceil(count / limit);
        return {data: user, pages};
    }

    static async getAlert(userId, alertId) {
        let err, user, alert;

        [err, user] = await to(User.findById(userId, 'alerts'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        alert = user.alerts.id(alertId);
        if (!alert) throw new Error(Message.NO_SUCH_ALERT);

        return {data: alert.toObject()};
    }

    static async deleteAlert(userId, alertId) {
        let err, user, alert;

        [err, user] = await to(User.findById(userId, 'alerts'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        alert = user.alerts.id(alertId).remove();
        if (!alert) throw new Error(Message.NO_SUCH_ALERT);

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: alert.toObject()};
    }

    static async updateAlert(userId, alertId, data) {
        let err, user, alert;

        [err, user] = await to(User.findById(userId, 'alerts'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        alert = user.alerts.id(alertId);
        if (!alert) throw new Error(Message.NO_SUCH_ALERT);

        alert.set(data);

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: alert.toObject()};
    }

    static async getNodesOfAlert(userId, alertId, page) {
        page = _.toNumber(page);
        if (!(_.isFinite(page) && page > 0)) {
            page = 1;
        }

        let err, user, alert;
        [err, user] = await to(User.findById(userId, 'alerts').populate({
            path: 'alerts',
            populate: {
                path: 'nodes'
            }
        }));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        alert = user.alerts.id(alertId);
        return {data:alert.nodes.toObject()};

    }
}

module.exports = {AlertService};