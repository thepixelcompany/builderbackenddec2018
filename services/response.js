const _ = require('lodash');

class Response {
    static error(message) {
        let r = {
            meta: {
                status: "fail",
                message
            }
        }
        return r;
    }

    static data(data) {
        let r = this.success();

        if (_.isObject(data) && !_.isArray(data)) {
            r = _.assign(r, data);
        } else {
            r.data = data;
        }

        return r;
    }

    static success() {
        let r = {
            meta: {
                status: "success"
            }
        }

        return r;
    }
}

module.exports = {Response};