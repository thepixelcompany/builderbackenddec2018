const {Config} = require('../models/config');
const to = require('../util/to');
const {picker} = require('../util/picker');

class ConfigService {
    static async getConfig() {
        let err, config;

        [err, config] = await to(Config.getInstance());
        if (err) {
            throw new Error(err);
        } else if (!config) {
            config = new Config();
            config = await config.save();
        }

        config = picker(config, ['token_exp']);
        return {data: config};
    }

    static async updateConfig(data) {
        let err, config;

        [err, config] = await to(Config.getInstance());
        if (err) {
            throw new Error(err);
        } else if (!config) {
            config = new Config(data);
            [err, config] = await to(config.save());
            if (err) throw new Error(err);
        }

        config = picker(config, ['token_exp']);
        return {data: config};
    }
}

module.exports = {ConfigService};