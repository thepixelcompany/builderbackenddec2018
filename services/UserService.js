'use strict';

const {User} = require('../models/user');
const {Token} = require('../models/token');
const {Node} = require('../models/node');
const ObjectId = require('mongoose').Types.ObjectId;
const to = require('../util/to');
const {Password} = require('../util/password');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const redis = require('../bin/redis');
const cache = require('../services/cache');
const {picker} = require('../util/picker');
const {Message} = require('./Message');
const util = require('util');
jwt.sign = util.promisify(jwt.sign);
jwt.verify = util.promisify(jwt.verify);

/**
 * This class includes the logic regarding the users and retrieves appropriate user documents from the DB.
 */
class UserService {

    /**
     * Retrieves users document from the DB, verifies the credentials and generates new authentication token.
     *
     * @param {String} email - The email of the user.
     * @param {String} password - The plain text password of the user.
     * @returns {Promise<{data: *, token: *}>} - The object which contains the user document and the
     * generated authentication token.
     * @author Tim
     */
    static async findByCredentials(email, password) {
        let err, user, token;

        [err, user] = await to(User.findOne({email}, '+password')/*.cache()*/);
        if (err) throw new Error(err);
        else if (!user) throw new Error(Message.NO_SUCH_USER);

        [err, user] = await to(user.comparePassword(password));
        if (err) throw new Error(err);

        [err, token] = await to(UserService.generateToken(user, false));
        if (err)
            throw new Error(err);
        else
            user = picker(user, ['_id', 'email', 'dashb_upd_freq', 'role']);
            return {user: {data: user}, token};
    }

    /**
     * Generates a new authentication token for the specified user. The token saves the id, email and role of the user.
     *
     * @param {Object<User>} user - The user document.
     * @returns {Promise<String>} - The new authentication token.
     * @author Tim
     */
    static async generateToken(user, oneTime) {
        if (!_.isBoolean(oneTime)) throw new TypeError(`Invalid type: ${oneTime}`);

        let err, token;

        // Stringify id if user is instance of its model
        if (user instanceof User) {
            user._id = user._id.toHexString();
        }

        const exp = (oneTime) ? '1d' : '10d';

        const data = {
            _id: user._id,
            email: user.email,
            role: user.role,
            oneTime: oneTime
        };

        [err, token] = await to(jwt.sign(data, 'secret', { expiresIn: exp}));
        if (err) throw new Error(err);

        let tokenDoc = new Token({user: user._id, token, oneTime: oneTime});
        [err, tokenDoc] = await to(tokenDoc.save());
        if (err) throw new Error(err);

        return token;
    }

    /**
     * Invalidates an authentication token through saving it to the redis cache for the standard exp. time of a token.
     * While the token is saved in cache every request which is done with this token is being blocked.
     *
     * @param {String} token - The encoded token to be invalidated in.
     * @author Tim
     */
    static async invalidateToken(token) {
        let err, res;

        [err, res] = await to(redis.hset('blockedTokens', token, 'true', 'EX', 10 * 60));
        if (err) throw new Error(err);
    }

    /**
     * Retrieves all users from the DB at the specified page.
     *
     * @param {Number} page - The page number.
     * @returns {Promise<{data: *, pages}>} - The list of users documents.
     * @author Tim
     */
    static async getAllUsers(page) {
        page = _.toNumber(page);
        if (!(_.isFinite(page) && page > 0)) {
            page = 1;
        }

        let limit = 20;
        let skip = limit * page - limit;
        let err, users, count;

        [err, users] = await to(
            User.find({}, null, {skip, limit})
                .cache()
        );
        if (err) throw new Error(err);

        [err, count] = await to(User.countDocuments());
        if (err) throw new Error(err);

        users = picker(users, ['_id', 'email', 'role', 'dashb_upd_freq']);
        let pagesCount = _.ceil(count / limit);

        return {data: users, pages: pagesCount};
    }

    /**
     * Creates a new user document and saves it to the DB.
     *
     * @param {Object} data - The data of the new user, which should always include 'email' and optionally 'role'.
     * @returns {Promise<Object<User>>} - The new users document.
     * @author Tim
     */
    static async createUser(data) {
        console.log(data)
        let err, password, hash, user, role, email, token;
        role = (data.role || 'basicUser');
        email = data.email;

        // Random password
        password = new Password(10);
        [err, hash] = await to(password.hash());
        if (err) throw new Error(err);

        let userObj = new User({email, password: hash, role, dashb_upd_freq: 5});
        [err, user] = await to(userObj.save());
        if (err) throw new Error(err);

        [err, token] = await to(this.generateToken(user, true));
        if (err) throw new Error(err);

        console.log(`OneTime token: ${token}`);

        user = picker(user, ['_id', 'email', 'dashb_upd_freq', 'role']);
        return {data: user};
    }

    static async deleteUser(id) {
        let err, user, tokens;
        [err, user] = await to(User.findByIdAndDelete(id));

        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        [err, tokens] = await to(Token.find({user: id}));
        if (err) throw new Error(err);

        for (let tokenDoc of tokens) {
            console.log(` + Invalidating token ${tokenDoc.token}`);
            [err] = await to(this.invalidateToken(tokenDoc.token));
            if (err) throw new Error(err);
        }

        // TODO send notification to admins

        return {data: user};
    }

    /**
     * Updates the specified users document and saves it back to the DB.
     *
     * @param {String} id - The id of the user to be updated.
     * @param {Object} data - The new data to be set.
     * @returns {Promise<Object<User>>} - The updated users document.
     * @author Tim.
     */
    static async updateUser(id, data) {
        if (!_.isObject(data)) throw new Error('Invalid data');

        let err, containsPassword, password, user, hash, tokens;
        containsPassword = await this.containsPassword(data);

        // Hash password
        if (containsPassword) {
            password = new Password(data.password);
            [err, hash] = await to(password.hash());
            if (err) throw new Error(err);

            data.password = hash;
        }

        [err, user] = await to(
            User.findOneAndUpdate({_id: id}, data, {new: true})
                .select('email dashb_upd_freq role _id')
        );

        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        [err, tokens] = await to(Token.find({user: user._id}));
        if (err) throw new Error(err);

        for (let tokenDoc of tokens) {
            console.log(` + Invalidating token ${tokenDoc.token}`);
            [err] = await to(this.invalidateToken(tokenDoc.token));
            if (err) throw new Error(err);
        }

        return {data: user};
    }

    static async containsPassword(data) {
        if (data.hasOwnProperty('password')) {
                delete data['password_repeat'];
                return true;
        }
        return false;
    }

    /**
     * Retrieves the user with the specified id.
     *
     * @param {String} id - The id of the user to be retrieved.
     * @returns {Promise<Object<User>>} - The users document.
     * @author Tim
     */
    static async getUser(id) {
        id = new ObjectId(id);

        let [err, user] = await to(User.findById(id)/*.cache()*/);
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        user = picker(user, ['_id', 'email', 'dashb_upd_freq', 'role']);
        return {data: user};
    }

    /**
     * Resets the password of the user with the specified email address by generating a new random password and saving
     * into users document. An email is sent to the user.
     *
     * @param {String} email - The email address of the user.
     * @returns {Promise<boolean>} - True if password has been reset.
     * @author Tim
     */
    static async resetPassword(email) {
        let err, user, hash;

        [err, user] = await to(User.findOne({email}));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        // TODO send email


        // Generates new random password.
        let password = new Password(10);
        [err, hash] = await to(password.hash());
        if (err) throw new Error(err);

        user.password = hash;

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return true;
    }

    static async getNotifications(userId, page) {
        page = _.toNumber(page);
        if (!(_.isFinite(page) && page > 0)) {
            page = 1;   // Sets default page.
        }

        const limit = 10;
        const skip = limit * page - limit;
        let err, user, count, aggr;

        // Retrieves the appropriate notifications page.
        [err, user] = await to(User.findById(userId, {notifications: { $slice: [skip, limit] }}));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        // Counts all notifications of the user.
        [err, aggr] = await to(User.aggregate([
            {$project: {_id: '$_id',  count: {$size: '$notifications'}}},
            {$match: {_id: new ObjectId(userId)}}
        ]));
        if (err) throw new Error(err);

        count = aggr[0].count;
        let pages = _.ceil(count / limit);
        return {data: user.notifications, pages};
    }

    static async deleteNotification(userId, notificationId) {
        let err, user, notification;

        [err, user] = await to(User.findById(userId, 'notifications'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        notification = user.notifications.id(notificationId).remove();
        if (!notification) throw new Error('No such notification');

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: notification.toObject()};
    }

    static async getNotification(userId, notificationId) {
        let err, user, notification;

        [err, user] = await to(User.findById(userId, 'notifications'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        notification = user.notifications.id(notificationId);
        if (!notification) throw new Error('No such notification');

        return {data: notification.toObject()};
    }

    static async createNodesLocalName(userId, nodeId, data) {
        let err, user;

        [err, user] = await to(User.findById(userId, 'nodes nodes_local_names'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        const iOfNode = _.findIndex(user.nodes, n => _.isEqual(n.toHexString(), nodeId));
        if (iOfNode === -1) throw new Error('Node is not assigned to you.');

        const iOfLocalName = _.findIndex(user.nodes_local_names,
            n => _.isEqual(n.node.toHexString(), nodeId));
        if (iOfLocalName !== -1) throw new Error('Node already has a local name.');

        const localName = {name: data.name, node: nodeId};
        user.nodes_local_names.push(localName);

        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: localName};
    }

    static async deleteNodesLocalName(userId, nodeId) {
        let err, user;
        [err, user] = await to(User.findById(userId, 'nodes nodes_local_names'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        const iOfNode = _.findIndex(user.nodes, n => _.isEqual(n.toHexString(), nodeId));
        if (iOfNode === -1) throw new Error('Node is not assigned to you.');

        const localNames = user.nodes_local_names.map(n => n.toObject());
        const localName = _.remove(localNames, n => _.isEqual(n.node.toHexString(), nodeId));

        user.nodes_local_names = localNames;
        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: localName};
    }

    static async updateNodesLocalName(userId, nodesId, data) {
        let err, user;
        [err, user] = await to(User.findById(userId, 'nodes nodes_local_names'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        const iOfNode = _.findIndex(user.nodes.map(n => _.isEqual(n.toHexString(), nodesId)));
        if (iOfNode === -1) throw new Error('Node is not assigned to you');

        const localNames = user.nodes_local_names.map(n => n.toObject());
        const localName = _.find(localNames, n => _.isEqual(n.node.toHexString(), nodesId));
        if (!localName) throw new Error('No local name defined');

        localName.name = data.name;
        user.nodes_local_names = localNames;
        [err, user] = await to(user.save());
        if (err) throw new Error(err);

        return {data: localName};
    }

    static async unassignNode(userId, nodeId) {
        let err, user;

        [err, user] = await to(User.findById(userId, 'nodes'));
        if (err) throw new Error(err);

        const nodes = user.nodes.map(n => n.toHexString());
        const node = _.remove(nodes, n => _.isEqual(n, nodeId))[0];
        user.nodes = nodes;

        return user.save()
            .then(() => Node.findById(node))
            .then(nodeDoc => {
                return {data: nodeDoc}
            })
            .catch(e => new Error(e));
    }

    static async assignNode(userId, nodeId) {
        let err, user, node;

        [err, user] = await to(User.findById(userId, 'nodes'));
        if (err) {
            throw new Error(err);
        } else if (!user) {
            throw new Error(Message.NO_SUCH_USER);
        }

        let nodes = user.nodes.map(n => n.toHexString());
        if (_.includes(nodes, nodeId)) throw new Error('Node is already assigned to user');

        [err, node] = await to(Node.findById(nodeId));
        if (err) {
            throw new Error(err);
        } else if (!node) {
            throw new Error(Message.NO_SUCH_NODE);
        }

        nodes.push(nodeId);
        user.nodes = nodes;

        return user.save()
            .then(u => {
                return {data: u}
            })
            .catch(e => new Error(e));
    }
}

module.exports = {UserService};