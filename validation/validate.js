const Joi = require('joi');
const {Response} = require('../services/response');

const validate = (schema) => {
    return (req, res, next) => {
        const result = Joi.validate(req.body, schema);
        if (result.error) {
            res.status(401).send(Response.error(result.error.message));
        } else {
            next();
        }
    };
};

module.exports = validate;