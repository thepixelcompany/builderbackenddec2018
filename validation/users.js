const Joi = require('joi');

const alertId = Joi.string().required();
const condition = Joi.object().keys({
    type: Joi.string().valid('goes above', 'falls below', 'stops sending').required().label('Type'),
    kpi_type: Joi.string()
        .when('type', { is: ['goes above', 'falls below'], then: Joi.valid('power usage', 'CPU utilization',
                'RAM usage', 'swap memory usage', 'disk usage',
                'disk accesses', 'disk throughput', 'network throughput').required().label('KPI type') }),
    data_function: Joi.string()
        .when('type', { is: ['goes above', 'falls below'], then: Joi.valid('derivative', 'integral', 'linear').required().label('Data processing function') }),
    threshold: Joi.number()
        .when('type', { is: ['goes above', 'falls below'], then: Joi.required() }),
    time: Joi.number().min(0).required()
});

const rules = {
    updateUser: Joi.object().keys({
        email: Joi.string().email({minDomainAtoms: 2}).optional().label('Email address'),
        password: Joi.string().regex(/^[a-zA-Z0-9]{5,30}$/).optional().label('Password'),
        password_repeat: Joi.any().valid(Joi.ref('password')).optional().label('Password repeat'),
        dashb_upd_freq: Joi.number().integer().min(1).max(100).optional().label('Dashboard update frequency')
    }).with('password', 'password_repeat'),
    createUser: Joi.object().keys({
        email: Joi.string().email({minDomainAtoms: 2}).required(),
        role: Joi.string().valid('basicUser', 'extendedUser', 'admin').optional()
    }),
    createAlert: Joi.object().keys({
        name: Joi.string().alphanum().min(1).max(40).required(),
        timeout: Joi.number().integer().min(0).max(500).required(),
        email: Joi.string().email({minDomainAtoms: 2}).required(),
        nodes: Joi.array().min(1).items(alertId).required(),
        conditions: Joi.array().min(1).items(condition).required()
    })
};



module.exports = {rules};