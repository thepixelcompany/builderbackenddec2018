const db = require('./../bin/db');
const apiApp = require('./restApi');

apiApp.listen(4000, 'localhost', () => {
    console.log("-> Running api app on port 4000");
});
