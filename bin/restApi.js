const express = require('express');
const App = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
var path = require("path");
const uploadApi = require('../rest_endpoints/uploads.js');
const uploadMediaApi = require('../rest_endpoints/media.js');
const previewApi = require('../rest_endpoints/preview.js');


App.use(cors());
App.use(bodyParser.json());
const publicPath = path.join(__dirname + '/../upload');
App.use(express.static(publicPath))
App.use("/api/uploads", uploadApi);
App.use("/api/media", uploadMediaApi);
App.use("/api/preview", previewApi);

App.set('views', __dirname + '/../upload/src');
App.engine('html', require('ejs').renderFile);
App.set('view enging', 'ejs')
App.get('/preview', (req, res) => {
    res.render('index.html')
})
module.exports = App;