
var mysql = require('mysql');
var origin_connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
    password: ''
})

origin_connection.connect(err => {
    if (err) throw err;
    console.log('Connected')
    origin_connection.query('CREATE DATABASE IF NOT EXISTS pixelcode', (err, result) => {
        if (err) throw err;
        console.log('Database created!')
    })
})

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pixelcode'
})
function create_table () {
    connection.query("CREATE TABLE IF NOT EXISTS upload_tb (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, image_name VARCHAR(200) NOT NULL, image_path VARCHAR(255) NOT NULL)", (err, result) => {
        if (err) throw err;
        console.log('Upload table created!')
    })
}
setTimeout(create_table, 200);
module.export = connection