const redis = require('redis');
const redisURI = 'redis://127.0.0.1:6379';
const client = redis.createClient(redisURI);
const util = require('util');
client.hget = util.promisify(client.hget);
client.get = util.promisify(client.get);
client.hset = util.promisify(client.hset);
client.set = util.promisify(client.set);

module.exports = client;