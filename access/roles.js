const _ = require('lodash');
const {User} = require('../models/user');
const cache = require('../services/cache');
const to = require('../util/to');
const {Message} = require('../services/Message');

/**
 * Describes the access levels structure.
 *
 * @author Tim
 */

const self = (params) => {
    return _.isEqual(params.user._id, params.target._id);
};

const roles = {
    basicUser: {
        can: [{
            name: 'user:edit',
            when: self
        }, {
            name: 'user:inspect',
            when: self
        }, {
            name: 'user:delete',
            when: self
        }, {
            name: 'user:resetPassword',
            when: self
        }, {
            name: 'node:inspect',
            when: async (params) => {
                let [err, user] = await to(User.findOne({_id: params.user._id})/*.cache()*/);
                if (err) {
                    throw new Error(err);
                } else if (!user) {
                    throw new Error(Message.NO_SUCH_USER);
                }

                let nodes = user.nodes.map(n => n.toHexString());

                return _.indexOf(nodes, params.target._id) >= 0;
            }
        }, {
            name: 'alert:inspectAll',
            when: self
        }, {
            name: 'alert:delete',
            when: self
        }, {
            name: 'alert:inspect',
            when: self
        }, {
            name: 'alert:create',
            when: (params) => {
                const allowedProps = ['name', 'nodes', 'timeout', 'email', 'conditions'];
                const givenProps = _.keys(params.data);

                return _.isEqual(params.user._id, params.target._id)
                    && _.every(givenProps, e => _.includes(allowedProps, e));
            }
        }, {
            name: 'alert:edit',
            when: (params) => {
                const allowedProps = ['name', 'nodes', 'timeout', 'email', 'conditions'];
                const givenProps = _.keys(params.data);

                return _.isEqual(params.user._id, params.target._id)
                    && _.every(givenProps, e => _.includes(allowedProps, e));
            }
        }, {
            name: 'notification:inspectAll',
            when: self
        }, {
            name: 'notification:delete',
            when: self
        }, {
            name: 'notification:inspect',
            when: self
        }, {
            name: 'node:createLocalName',
            when: self
        }, {
            name: 'node:updateLocalName',
            when: self
        }, {
            name: 'node:deleteLocalName',
            when: self
        },
        'user:logout',
        'user:verify']
    },
    extendedUser: {
        inherits: ['basicUser'],
        can: ['user:inspectAll',
            'user:inspect',
            'node:inspectAll',
            'node:inspect',
            'node:unassign',
            'node:assign',
            {
                name: 'node:edit',
                when: (params) => {
                    const allowedProps = ['name', 'freq'];
                    const givenProps = _.keys(params.data);

                    return _.every(givenProps, e => _.includes(allowedProps, e));
                }
            }
        ]
    },
    admin: {
        inherits: ['extendedUser'],
        can: ['user:delete',
            'user:create',
            'node:delete',
            'user:edit',
            'config:inspect',
            'config:edit'
        ]
    }
};

module.exports = roles;