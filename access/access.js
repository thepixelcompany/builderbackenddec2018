const _ = require('lodash');
const roles = require('./roles');
const to = require('../util/to');
const redis = require('../bin/redis');
const jwt = require('jsonwebtoken');
const util = require('util');
const {UserService} = require('../services/UserService');
const {Response} = require('../services/response');
const {Message} = require('../services/Message');

jwt.sign = util.promisify(jwt.sign);
jwt.verify = util.promisify(jwt.verify);

class Access {
    constructor(roles) {
        this.init(roles);
    }

    init(roles) {
        let map = {};
        Object.keys(roles).forEach(role => {
            map[role] = {
                can: {}
            };

            if (roles[role].inherits) {
                map[role].inherits = roles[role].inherits;
            }

            roles[role].can.forEach(action => {
                if (_.isString(action)) {
                    map[role].can[action] = true;
                } else if (_.isString(action.name) && _.isFunction(action.when)) {
                    map[role].can[action.name] = action.when;
                }
            });

        });

        this.roles = map;
    }

    async can(role, operation, params) {
        let r = this.roles[role];

        if (r.can[operation]) {
            if (!_.isFunction(r.can[operation])) {
                return true;
            } else {

                // Checks "when" condition
                let allowed = await r.can[operation](params);
                if (allowed) {
                    return true;
                } else {
                    throw new Error();
                }
            }
        }

        if (!r.inherits || r.inherits.length < 1) {
            throw new Error();
        }

        // Lookup the parent roles
        for (let inheritedRole of r.inherits) {
            let [err, allowed] = await to(this.can(inheritedRole, operation, params));
            if (allowed) {
                return true;
            }
        }
        throw new Error();
    }

    expiresSoon(decodedToken) {
        const halfTime = (decodedToken.exp - decodedToken.iat) / 2 + decodedToken.iat;
        const now = Date.now() / 1000;
        return (now > halfTime);
    }

    async refreshToken(res, token, decodedToken) {
        if (this.expiresSoon(decodedToken)) {
            console.log(' ! Token expires soon');
            let freshToken = await to(UserService.generateToken(decodedToken, false));
            res.header('X-Auth', freshToken);
            let [err] = await to(redis.hset('blockedTokens', token, 'true', 'EX', 60 * 60 * 24));
            if (err) throw new Error(err);
        }
    }

    access(operation) {
        let access = this;
        return async (req, res, next) => {
            let err, decodedToken, can, isBlockedToken;
            let token = req.header('X-Auth');

            [err, isBlockedToken] = await to(redis.hget('blockedTokens', token));
            if (isBlockedToken) {
                return res.status(400).send(Response.error("Token was invalidated"));
            }

            [err, decodedToken] = await to(jwt.verify(token, 'secret', {}));
            if (err) {
                return res.status(400).send(Response.error(Message.INVALID_TOKEN));
            }

            if (decodedToken.oneTime) {
                // TODO redesign: What if user enters two different passwords
                [err, res] = await to(redis.hset('blockedTokens', token, 'true', 'EX', 60 * 60 * 24));
                if (err) throw new Error(err);
            } else {
                // TODO generate new one time token for sign up in case it expires
                await this.refreshToken(res, token, decodedToken);
            }

            let params = {
                user: {},
                target: {},
                data: {}
            };

            params.user._id = decodedToken._id;
            params.data = req.body;
            if (!_.isNull(req.params._id)) {
                params.target._id = req.params.id;
            }

            [err, can] = await to(access.can(decodedToken.role, operation, params));
            if (err) {
                return res.status(400).send(Response.error(Message.NO_PERMISSION));
            }
            console.log(` + Access granted: ${operation}`);

            // Sets user id for future processing
            req.user = {
                _id: decodedToken._id
            };

            next();
        };
    }
}


let AccessController = new Access(roles);

module.exports = AccessController;